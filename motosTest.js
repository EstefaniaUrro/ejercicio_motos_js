class Moto{
  constructor(marca,precio,km,cc){
    this.marca = marca;
    this.precio = precio;
    this.km = km;
    this.cc = cc;
  }
}
  let lista = [
     new Moto("DUCATI",1300,68000,249),
     new Moto("SUZUKI",1200,45000,200),
     new Moto("HONDA",1599,15000,250),
     new Moto("HONDA",1600,49000,152),
     new Moto("YAMAHA",1890,22000,395),
     new Moto("DUCATI",2300,28000,350),
     new Moto("DUCATI",2320,28000,350),
     new Moto("HONDA",1400,45000,150),
     new Moto("HONDA",1350,55000,153),
     new Moto("HONDA",1420,44000,150),
     new Moto("DUCATI",2000,24000,355) 
]; 

let ordenar = lista.sort(ordenarPorCosto);


function ordenarPorCosto(a,b){
    if(a.precio === b.precio) return 0;
      if(a.precio > b.precio) return 1;
      return -1;
    }
  

console.log("La moto mas barata es " + ordenar[0].marca + " con " + ordenar[0].precio + "€");
console.log("La moto mas cara es " + ordenar[lista.length-1].marca + " con " + ordenar[lista.length-1].precio + "€");
console.log("¿Cuántas motos hay con menos de 30.000 km de la marca HONDA?");
console.log(ordenar.filter(p=>p.km<=30000).filter(p=>p.marca<="HONDA"));
console.log("¿Cuántas motos hay con menos de 30.000 km de más de 240cc?");
console.log(ordenar.filter(p=>p.km<=30000).filter(p=>p.cc>=240));
console.log("¿Qué moto tiene menos de 25.000 km, más de 350cc de cilindrada y cuesta entre 1.800 y 2.200 eur?");
console.log(ordenar.filter(p=>p.precio>=1800 && p.precio<=2200).filter(p=>p.km<=25000).filter(p=>p.cc>=350));

let nombres = lista.reduce((contador,unaMoto) => {
    contador[unaMoto.marca] = (contador[unaMoto.marca] || 0) + 1 ;
    return contador;
},{});
console.log("Nombres " +nombres);

salida = [];
for(moto in lista){
    salida[moto.marca] = (salida[moto.marca] || 0) + 1;
}

console.log(salida);



  





